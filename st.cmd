# ---
require essioc
require esipressuregauge

epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(IPADDR, "172.30.32.57")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(PM8006_ADDR, "F7")
epicsEnvSet(SYSTEM, "SE-SEE")
epicsEnvSet(DEVICE, "SE-PTRANS-001")
epicsEnvSet(PREFIX, "$(SYSTEM):$(DEVICE):")
epicsEnvSet(LOCATION, "E03.110; $(IPADDR)")
epicsEnvSet(IOCNAME, "$(SYSTEM):$(DEVICE)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(esipressuregauge_DIR)db")

# E3 Common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load module databases
iocshLoad("$(esipressuregauge_DIR)/esiPressureGauge.iocsh")
# ...
